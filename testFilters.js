const Web3 = require('web3')
const url = process.env['URL']

const reset = '\x1b[0m'
const red = '\x1b[31m'
const green = '\x1b[32m'
const blue = '\x1b[36m'

const p = path => path.split('/').pop()

const e = err => {
  console.error(err)
  process.exit(9)
}

let lastBlock = null
let filterBlock = null

const showBlock = (err, block, isFilter) => {
  const type = (isFilter) ? 'Filter' : 'getBlock'

  if (err) {
    console.error(err)
  } else {
    const number = block.number || block.blockNumber
    if (block && number) {

      if (isFilter) filterBlock = number
      else lastBlock = number

      let color = (filterBlock === lastBlock) ? green : red
      let date = Date.now()
      console.log(`[${date}] ${color} LastBlock = ${lastBlock}, reported by filter: ${filterBlock} ${reset}`)
    } else {
      console.error(`${type} Error:`)
      console.dir(block, { colors: true })
    }
  }
}

if (!url) e(`Use: URL='[node-url]' ${p(process.argv[0])} ${p(process.argv[1])}`)

const web3 = new Web3(new Web3.providers.HttpProvider(url))
if (!web3.isConnected()) e('Web is not connected')

console.log(`${blue} Node version: ${web3.version.node} ${reset}`)

// get block to check latest block
const interval = setInterval(() => {
  const block = web3.eth.getBlock('latest', (err, block) => {
    showBlock(err, block)
  })
}, 3000)

// set filter to listen for new blocks
web3.reset(true)
console.log('Listen to blocks')
const filter = web3.eth.filter('latest')
filter.watch((err, hash) => {
  if (err) {
    console.error(`${red} Filter ERROR: ${err}`)
  } else {
    console.log(`${blue} Filter hash:${hash}`)
    web3.eth.getBlock(hash, (err, block) => {
      showBlock(err, block, true)
    })
  }
})



